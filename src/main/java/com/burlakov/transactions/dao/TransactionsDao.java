package com.burlakov.transactions.dao;

import com.burlakov.transactions.entities.Transaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by denysburlakov on 11.10.2017.
 */
public interface TransactionsDao extends CrudRepository<Transaction, Long> {

}
