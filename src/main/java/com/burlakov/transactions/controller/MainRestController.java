package com.burlakov.transactions.controller;

import com.burlakov.transactions.dao.TransactionsDao;
import com.burlakov.transactions.entities.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denysburlakov on 10.10.2017.
 */

@RestController
public class MainRestController {
    @Autowired
    private TransactionsDao transactionsDao;

    @RequestMapping("/transactions")
    @ResponseBody
    public List<Transaction> listTransactions() {
        List<Transaction> transactions = new ArrayList<>();
        transactionsDao.findAll().forEach(transactions::add);
        return transactions;
    }

    @RequestMapping("/transaction/{id}")
    @ResponseBody
    public Transaction viewTransaction(@PathVariable String id) {
        return transactionsDao.findOne(Long.valueOf(id));
    }

}
