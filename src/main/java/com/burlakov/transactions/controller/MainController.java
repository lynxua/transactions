package com.burlakov.transactions.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by denysburlakov on 15.10.2017.
 */
@Controller
public class MainController {

    @RequestMapping("/")
    public String main(){
        return "transactions_page";
    }
}
